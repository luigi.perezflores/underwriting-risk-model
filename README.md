# Underwriting Risk Model

Solution to MoneyLion challenge for Data Science position.

Create a risk score to reduce portfolio risk exposure 

## Contents
- [Data Source](#data-source)
- [Dependencies](#dependencies)
- [Notebooks](#notebooks)
- [Model KPS]()

## Data Source
AWS S3:

https://us-east-1.console.aws.amazon.com/s3/buckets/moneylion-project?region=us-east-1&bucketType=general&tab=objects


1. loan.csv
2. payment.csv
3. clarity_underwriting_variables.csv

## Dependencies

- Python: 3.11.4
- Install requirements.txt
- Credentials: Create `config/credentials.ini` file with user and pwd for AWS s3 certifications

## Notebooks

The proyect is developed in Jupyter Notebooks:

- 1_EDA.ipynb : 
  1. Data analysis
  2. Missing Treatment
  3. Merge tables
  4. Target Definition

- 2_Modeling.ipynb : 
  1. Train/test split
  2. Feature reduction
  3. Modeling
  4. Reporting


## Model KPIs

Confusion Matrix:

||Accuracy|	Precision| Recall|Specificity|	F1|	AUC|	AUCPR|
|-|-|-|-|-|-|-|-|
|Train|	69.6|	70.9|	72.0|	67.0|71.4|	76.9|	78.7|
|Test|	64.4|	65.6|	68.4|	60.0|67.0|	70.6|	73.6|

Deciles:

||KS	|Weighted_MAPE|	Decile9_MAPE|
|-|-|-|-|
|Train	|0.3918|	0.091542|	0.094474|
|Test	|0.2925|	0.052621|	0.041842|


Business:

|	|Current|Optimized|	diff|
|-|-|-|-|
|Loans|4376	|3,924	 |-452|
|LoanAmount|	2,745,459|	2,445,184	|-300,275|
|NetMargin|	147,131.31|	323,609.17	|176,477.86|
|NetMargin_pct|	5.36%|	13.23%	|7.87%|
|Margin_per_client|	33.62|	82.47	|48.85|

