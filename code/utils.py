import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

def null_detection(df:pd.DataFrame):
    # Count of nulls per column
    null_count_per_column = df.isnull().sum()

    # Percentage of nulls per column
    total_rows = len(df)
    null_percentage_per_column = (null_count_per_column / total_rows) * 100
    
    print("Null count per column:")
    print(null_count_per_column[null_count_per_column > 0].sort_values(ascending=False))
    print("\nPercentage of nulls per column:")
    print(null_percentage_per_column[null_percentage_per_column > 0].sort_values(ascending=False).round(2))

def plot_numeric_columns(df:pd.DataFrame):
    # Get numeric columns
    numeric_columns = df.select_dtypes(include=['number','bool']).columns

    # Calculate number of rows needed for plotting
    num_plots = len(numeric_columns)
    num_rows = (num_plots + 2) // 3

    # Create subplots
    fig, axes = plt.subplots(num_rows, 3, figsize=(12, num_rows*5))

    # Flatten axes to make it easier to iterate
    axes = axes.flatten()

    # Plot histograms
    for i, col in enumerate(numeric_columns):
        sns.histplot(df[col], ax=axes[i], stat='percent', bins=20)
        axes[i].set_title(col)

    # Remove empty subplots
    for j in range(num_plots, num_rows*3):
        fig.delaxes(axes[j])

    plt.tight_layout()
    plt.show()

def plot_categorical(df:pd.DataFrame, columns:list):

    # Calculate number of rows needed for plotting
    num_plots = len(columns)
    num_rows = len(columns)

    # Create subplots
    fig, axes = plt.subplots(num_rows, 1, figsize=(10, num_rows*4))

    # Flatten axes to make it easier to iterate
    axes = axes.flatten()

    # Plot histograms
    for i, col in enumerate(columns):
        sns.barplot(df[col].value_counts(normalize=True), ax=axes[i])
        axes[i].set_title(col)
        axes[i].tick_params(axis='x', rotation=45)  # Rotate x-axis labels


    plt.tight_layout()
    plt.show()