import numpy as np
import pandas as pd
#from sklearn.feature_selection import SelectKBest, f_classif
#from sklearn.linear_model import LogisticRegression
from scipy.stats import chi2_contingency

def iv(df:pd.DataFrame,features_list:list,target:str, threshold:float=.02):
    df_=df.copy()
    lst_var=[]
    lst_iv=[]

    dummy_variables = df[features_list].nunique()[df[features_list].nunique() <= 4].index.tolist()
    num_variables = df[features_list].nunique()[df[features_list].nunique() > 4].index.tolist()

    # IV for Numeric Variables
    for var in num_variables:
        # Create bins for each variable
        _, bins = pd.qcut(df_[var], q=10, retbins=True, duplicates='drop')

        # Create a new column with binned values
        df_[var + '_bin'] = pd.cut(df_[var], bins=bins, include_lowest=True)

        # Calculate WoE (Weight of Evidence) and IV
        grouped = df_.groupby(var + '_bin',observed=False)[target].agg(['count', 'sum'])
        grouped['non_event'] = grouped['count'] - grouped['sum']
        grouped['event_rate'] = grouped['sum'] / grouped['sum'].sum()
        grouped['non_event_rate'] = grouped['non_event'] / grouped['non_event'].sum()
        grouped['woe'] = np.log(grouped['event_rate'] / grouped['non_event_rate'])
        grouped['iv'] = (grouped['event_rate'] - grouped['non_event_rate']) * grouped['woe'].replace({np.inf: 0, -np.inf: 0})

        # Sum IV for each variable
        iv = grouped['iv'].sum()

        # Append the results to the IV DataFrame
        lst_var.append(var)
        lst_iv.append(iv)

    # IV for Categorical Variables
    for var in dummy_variables:
        grouped = pd.crosstab(df[var], df[target])
        non_event_percent = grouped[0] / grouped[0].sum()
        event_percent = grouped[1] / grouped[1].sum()
        grouped["woe"] = np.log(event_percent / non_event_percent)
        grouped["iv"] = (event_percent - non_event_percent) * grouped["woe"].replace({np.inf: 0, -np.inf: 0})

        # Sum IV for each variable
        iv = grouped['iv'].sum()

        # Append the results to the IV DataFrame
        lst_var.append(var)
        lst_iv.append(iv)


    iv_table = pd.DataFrame({'variable':lst_var, 'iv':lst_iv})
    iv_table = iv_table.sort_values(by="iv",ascending=False).reset_index(drop=True)
    drop_var_iv = iv_table[iv_table["iv"]<=threshold]["variable"].to_list()
    return iv_table, drop_var_iv

def fit_deciles(df:pd.DataFrame,var:str,n:int=10):
    _, bins = pd.qcut(df[var], q=n, retbins=True,duplicates="drop")
    return bins

def transform_deciles(df:pd.DataFrame, var:str, bins:int,n:int=10):
    tmp_dec = pd.cut(df[var], bins=bins, labels=False, include_lowest=True)
    df['decile'] = tmp_dec
    return df

def csi(df_fit:pd.DataFrame, df_test:pd.DataFrame, features_list:list, n_bins:int=10, threshold:float=.10):
    dev_ = df_fit.copy()
    itv_ = df_test.copy()
    csi_deciles =[]
    for var in features_list:
        bins = fit_deciles(df=dev_,var=var,n=n_bins)
        dev_by_decile = transform_deciles(df=dev_,var=var,bins=bins,n=n_bins)
        itv_by_decile = transform_deciles(df=itv_,var=var,bins=bins,n=n_bins)

        csi_table1 = dev_by_decile.groupby("decile").agg({"decile":"count"})
        csi_table1.columns=["distr_dev_"]
        csi_table1["distr_dev_"]=csi_table1["distr_dev_"]/csi_table1["distr_dev_"].sum()
        csi_table1

        csi_table2 = itv_by_decile.groupby("decile").agg({"decile":"count"})
        csi_table2.columns=["distr_itv_"]
        csi_table2["distr_itv_"]=csi_table2["distr_itv_"]/csi_table2["distr_itv_"].sum()
        csi_table2

        csi_table = pd.concat([csi_table1 ,csi_table2],axis=1)
        csi_table["diff"] = csi_table["distr_dev_"] - csi_table["distr_itv_"]
        csi_table["ln_dev_/itv_"] = np.log(csi_table["distr_dev_"]/csi_table["distr_itv_"])
        csi_table["csi"] = csi_table["diff"] * csi_table["ln_dev_/itv_"]
        csi_table["variable"]=var
        csi_deciles.append(csi_table)
    csi_deciles = pd.concat(csi_deciles)
    csi_deciles = csi_deciles.reset_index(drop=False)
    csi_table = csi_deciles.groupby("variable").agg({"csi":"sum"}).reset_index(drop=False)
    drop_var_csi = csi_table[csi_table["csi"]>threshold]["variable"].to_list()

    return csi_table, drop_var_csi

def chi2_test(df:pd.DataFrame, features_list:list, target:str, pvalue:float=.05):
    lst_var = []
    lst_chi2 = []

    for var in features_list:
        # contingency_table
        contingency_table = pd.crosstab(df[var],df[target])

        # Perform Chi-Square test
        _, pval, _, _ = chi2_contingency(contingency_table)
        
        # Append the results to the IV DataFrame
        lst_var.append(var)
        lst_chi2.append(pval.round(5))

        chi2_table = pd.DataFrame({'variable':lst_var, 'pval':lst_chi2})
        chi2_table = chi2_table.sort_values(by="pval",ascending=True).reset_index(drop=True)
        drop_var_chi2 = chi2_table[chi2_table["pval"]>pvalue]["variable"].to_list()

    return chi2_table, drop_var_chi2