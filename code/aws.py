import boto3
from io import StringIO
from botocore.exceptions import ClientError

import pandas as pd

class s3_bucket():
    def __init__(
            self, 
            aws_access_key_id:str,
            aws_secret_access_key:str,
            bucket_name:str,
            region_name:str="us-east-1",
            ):

        self.bucket_name = bucket_name

        self.s3_client = boto3.client(
            service_name = 's3',
            region_name = region_name,
            aws_access_key_id = aws_access_key_id,
            aws_secret_access_key = aws_secret_access_key
        )
        try:
            self.s3_client.head_bucket(Bucket=self.bucket_name)
        except ClientError as e:
            error_code = int(e.response['Error']['Code'])
            if error_code == 404:
                print(f"Bucket '{self.bucket_name}' not found")
            raise
        
    ## SHOW
    def show_objects(self):
        lst = []
        s3_objects = self.s3_client.list_objects_v2(Bucket=self.bucket_name)
        for obj in s3_objects['Contents']:
            lst.append(obj['Key'])
        return lst 

    ## CHECK DATA
    def check_file_exists(self, file_key):
        try:
            self.s3_client.head_object(Bucket=self.bucket_name, Key=file_key)
            return True
        except ClientError as e:
            if e.response['Error']['Code'] == '404':
                return False
            else:
                raise

    ## WRITE
    def upload_objects(self, files_to_upload:list):
        """Upload a list of objects to s3 bucket

        Args:
            files_to_upload (list): list of tuples (local_file, s3_file name)
        """
        for local_file, s3_key in files_to_upload:
            # Check if the file already exists in the bucket
            try:
                self.s3_client.head_object(Bucket=self.bucket_name, Key=s3_key)
                print(f"File '{s3_key}' already exists.")
            except Exception as e:
                self.s3_client.upload_file(local_file, self.bucket_name, s3_key)
                print(f"File '{s3_key}' uploaded successfully.")
        return

    def upload_df_as_csv(self, lst:list,replace:bool=False):
        for df, file_key in lst:
            # Check if the file already exists
            if not self.check_file_exists(file_key):
                # Convert DataFrame to CSV in memory
                csv_buffer = StringIO()
                df.to_csv(csv_buffer, index=False)
                
                # Upload the CSV to S3
                self.s3_client.put_object(Bucket=self.bucket_name, Key=file_key, Body=csv_buffer.getvalue())
                print(f"File '{file_key}' uploaded successfully to S3.")
            elif replace:
                # Convert DataFrame to CSV in memory
                csv_buffer = StringIO()
                df.to_csv(csv_buffer, index=False)
                
                # Upload the CSV to S3
                self.s3_client.put_object(Bucket=self.bucket_name, Key=file_key, Body=csv_buffer.getvalue())
                print(f"File '{file_key}' already exists in bucket. File replaced.")
            else:
                print(f"File '{file_key}' already exists in bucket. Upload skipped.")

    ## READ
    def read_csv(self, file:str):
        """Read a csv file into a pandas df

        Args:
            file (str): s3 file name

        Returns:
            pd.DataFrame: csv file converted into DataFrame
        """
        obj = self.s3_client.get_object(Bucket=self.bucket_name, Key=file)
        return pd.read_csv(obj['Body'], low_memory=False)